package com.eva.biz.system;

import com.eva.dao.system.dto.CreateSystemUserDTO;
import com.eva.dao.system.dto.CreateUserRoleDTO;
import com.eva.dao.system.dto.ResetSystemUserPwdDTO;
import com.eva.dao.system.dto.UpdatePwdDto;

import java.util.List;

/**
 * 系统用户业务处理
 * @author Eva.Caesar Liu
 * @since 2022/05/09 19:56
 */
public interface SystemUserBiz {

    /**
     * 删除
     *
     * @param id 用户ID
     */
    void deleteById(Integer id);

    /**
     * 批量删除
     *
     * @param ids 用户ID列表
     */
    void deleteByIdInBatch(List<Integer> ids);

    /**
     * 修改密码
     *
     * @param dto 详见UpdatePwdDto
     */
    void updatePwd(UpdatePwdDto dto);

    /**
     * 重置密码
     *
     * @param dto 详见ResetSystemUserPwdDTO
     */
    void resetPwd(ResetSystemUserPwdDTO dto);

    /**
     * 创建用户
     *
     * @param systemUser 详见CreateSystemUserDTO
     */
    void create(CreateSystemUserDTO systemUser);

    /**
     * 修改用户信息
     *
     * @param systemUser 详见CreateSystemUserDTO
     */
    void updateById(CreateSystemUserDTO systemUser);

    /**
     * 创建用户角色
     *
     * @param dto 详见CreateUserRoleDTO
     */
    void createUserRole(CreateUserRoleDTO dto);
}
