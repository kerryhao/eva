package com.eva.service.system;

import com.eva.core.model.PageData;
import com.eva.core.model.PageWrap;
import com.eva.dao.system.dto.QuerySystemLoginLogDTO;
import com.eva.dao.system.model.SystemLoginLog;
import java.util.List;

/**
 * 登录日志Service定义
 * @author Eva.Caesar Liu
 * @since 2022/05/09 19:56
 */
public interface SystemLoginLogService {

    /**
     * 创建
     *
     * @param systemLoginLog 实体
     * @return Integer
     */
    Integer create(SystemLoginLog systemLoginLog);

    /**
     * 主键删除
     *
     * @param id 主键
     */
    void deleteById(Integer id);

    /**
     * 删除
     *
     * @param systemLoginLog 删除条件
     */
    void delete(SystemLoginLog systemLoginLog);

    /**
     * 批量主键删除
     *
     * @param ids 注解列表
     */
    void deleteByIdInBatch(List<Integer> ids);

    /**
     * 主键更新
     *
     * @param systemLoginLog 实体
     */
    void updateById(SystemLoginLog systemLoginLog);

    /**
     * 批量主键更新
     *
     * @param systemLoginLogs 实体列表
     */
    void updateByIdInBatch(List<SystemLoginLog> systemLoginLogs);

    /**
     * 主键查询
     *
     * @param id 主键
     * @return SystemLoginLog
     */
    SystemLoginLog findById(Integer id);

    /**
     * 条件查询单条记录
     *
     * @param systemLoginLog 查询条件
     * @return SystemLoginLog
     */
    SystemLoginLog findOne(SystemLoginLog systemLoginLog);

    /**
     * 条件查询
     *
     * @param systemLoginLog 查询条件
     * @return List<SystemLoginLog>
     */
    List<SystemLoginLog> findList(SystemLoginLog systemLoginLog);

    /**
     * 分页查询
     *
     * @param pageWrap 分页参数
     * @return PageData<SystemLoginLog>
     */
    PageData<SystemLoginLog> findPage(PageWrap<QuerySystemLoginLogDTO> pageWrap);

    /**
     * 条件统计
     *
     * @param systemLoginLog 统计条件
     * @return long
     */
    long count(SystemLoginLog systemLoginLog);
}
